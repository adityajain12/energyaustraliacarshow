package com.example.energy.australia.model;

public class CarDetails implements Comparable<CarDetails> {

	String carShowName;
	String make;
	String model;
	public String getCarShowName() {
		return carShowName;
	}
	public void setCarShowName(String carShowName) {
		this.carShowName = carShowName;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@Override
	public String toString() {
		//return  carShowName  + "\n            " + make+ ", model=" + model + "]";
		return "\n"+ make  + "\n      "+model + "\n            "+carShowName;
	}
	@Override
	public int compareTo(CarDetails c1) {
		// TODO Auto-generated method stub
		return this.make.compareTo(c1.make);
		//return 0;
	}
	
	
}
