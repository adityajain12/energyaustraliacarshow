package com.example.energy.australia.carShow;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;






import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import com.example.energy.australia.model.CarDetails;



@SpringBootApplication
public class CarApps implements CommandLineRunner{

	public static void main(String[] args) {
		try{
		SpringApplication.run(CarApps.class, args);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public void run(String... arg0) throws Exception {
		// TODO Auto-generated method stub
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(org.springframework.http.MediaType.APPLICATION_JSON));
		
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		String url = "http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars";   //Resource URL
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		System.out.println("responseEntity- body-->"+responseEntity.getBody());
		System.out.println("Response Headers--->"+responseEntity.getHeaders());
		String status = responseEntity.getStatusCode().toString().split(" ")[0];
		int statusOk = HttpServletResponse.SC_OK;
		//System.out.println("empty check--->"+responseEntity.getBody().toString().trim().length());
		// hardcoded if service is unavailable 
		//String hardCoded = "[{\"name\":\"New York Car Show\",\"cars\":[{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"George Motors\",\"model\":\"George 15\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 1\"},{\"make\":\"Moto Tourismo\",\"model\":\"Cyclissimo\"},{\"make\":\"Edison Motors\",\"model\":\"\"}]},{\"name\":\"Melbourne Motor Show\",\"cars\":[{\"make\":\"Julio Mechannica\",\"model\":\"Mark 4S\"},{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"Moto Tourismo\",\"model\":\"Cyclissimo\"},{\"make\":\"George Motors\",\"model\":\"George 15\"},{\"make\":\"Moto Tourismo\",\"model\":\"Delta 4\"}]},{\"name\":\"Cartopia\",\"cars\":[{\"make\":\"Moto Tourismo\",\"model\":\"Cyclissimo\"},{\"make\":\"George Motors\",\"model\":\"George 15\"},{\"make\":\"Hondaka\",\"model\":\"Ellen\"},{\"make\":\"Moto Tourismo\",\"model\":\"Delta 16\"},{\"make\":\"Moto Tourismo\",\"model\":\"Delta 4\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 2\"}]},{\"name\":\"Carographic\",\"cars\":[{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"Hondaka\",\"model\":\"Elisa\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 4\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 2\"},{\"make\":\"Moto Tourismo\"},{\"make\":\"Julio Mechannica\",\"model\":\"Mark 4\"}]},{\"cars\":[{\"make\":\"Moto Tourismo\",\"model\":\"Delta 4\"}]}]";
		//JSONArray arrJson = new JSONArray(hardCoded);
		
		//In below line, we are checking whether response is successful or not.
		if(status.equals(Integer.toString(statusOk)))
		{
			JSONArray arrJson = new JSONArray(responseEntity.getBody());
			List<CarDetails> listCarDetais =null;
			JSONArray arrJsonCars = null;
			CarDetails carDetails = null;
			listCarDetais = new ArrayList<>();
			//In below loop we are iterating Json Array so that we able to get CarShow and cars details respectively.
			for(int i =0; i<arrJson.length();i++)
			{
				JSONObject jsonObj = arrJson.getJSONObject(i);
					String carShowName = "NA";
					if(jsonObj.length()!=1)
						carShowName = (String) jsonObj.get("name");
					// In below loop we able to get make and model od cars respective to a carShow.
					arrJsonCars = new JSONArray(jsonObj.get("cars")+"");
					for(int j =0; j<arrJsonCars.length();j++)
					{
						JSONObject jsonCarObject = arrJsonCars.getJSONObject(j);
						String make = (String) jsonCarObject.get("make");
						String model = "NA";
						if(jsonCarObject.length()!=1)
						 model = (String) jsonCarObject.get("model");
						// Model Creation
						 carDetails = new CarDetails();
						carDetails.setCarShowName(carShowName);
						carDetails.setMake(make);
						carDetails.setModel(model);
						listCarDetais.add(carDetails);
					}
			}	
		//System.out.println("listCarDetais--Before--Sorting---->"+listCarDetais);
		Collections.sort(listCarDetais);
		System.out.println("listCarDetais--After---Sorting---->"+listCarDetais);
		}
		else
		System.out.println("Service is not avaialble or repsonse Body is empty");
	}
}
